import { useEffect, useState } from "react";
import styled from "styled-components";
import Countdown from "react-countdown";
import { Button, CircularProgress, Snackbar } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";

import * as anchor from "@project-serum/anchor";

import { LAMPORTS_PER_SOL } from "@solana/web3.js";

import { useAnchorWallet } from "@solana/wallet-adapter-react";
import { WalletDialogButton } from "@solana/wallet-adapter-material-ui";

import {
  CandyMachine,
  awaitTransactionSignatureConfirmation,
  getCandyMachineState,
  mintOneToken,
  shortenAddress,
} from "./candy-machine";

const ConnectButton = styled(WalletDialogButton)``;

const CounterText = styled.span``; // add your styles here

const MintButton = styled(Button)``; // add your styles here

export interface HomeProps {
  candyMachineId: anchor.web3.PublicKey;
  config: anchor.web3.PublicKey;
  connection: anchor.web3.Connection;
  startDate: number;
  treasury: anchor.web3.PublicKey;
  txTimeout: number;
}

const Home = (props: HomeProps) => {
  const [balance, setBalance] = useState<number>();
  const [isActive, setIsActive] = useState(false); // true when countdown completes
  const [isSoldOut, setIsSoldOut] = useState(false); // true when items remaining is zero
  const [isMinting, setIsMinting] = useState(false); // true when user got to press MINT

  const [itemsAvailable, setItemsAvailable] = useState(0);
  const [itemsRedeemed, setItemsRedeemed] = useState(0);
  const [itemsRemaining, setItemsRemaining] = useState(0);

  const [alertState, setAlertState] = useState<AlertState>({
    open: false,
    message: "",
    severity: undefined,
  });

  const [startDate, setStartDate] = useState(new Date(props.startDate));

  const wallet = useAnchorWallet();
  const [candyMachine, setCandyMachine] = useState<CandyMachine>();

  const refreshCandyMachineState = () => {
    (async () => {
      if (!wallet) return;

      const {
        candyMachine,
        goLiveDate,
        itemsAvailable,
        itemsRemaining,
        itemsRedeemed,
      } = await getCandyMachineState(
        wallet as anchor.Wallet,
        props.candyMachineId,
        props.connection
      );

      setItemsAvailable(itemsAvailable);
      setItemsRemaining(itemsRemaining);
      setItemsRedeemed(itemsRedeemed);

      setIsSoldOut(itemsRemaining === 0);
      setStartDate(goLiveDate);
      setCandyMachine(candyMachine);
    })();
  };

  const onMint = async () => {
    try {
      setIsMinting(true);
      if (wallet && candyMachine?.program) {
        const mintTxId = await mintOneToken(
          candyMachine,
          props.config,
          wallet.publicKey,
          props.treasury
        );

        const status = await awaitTransactionSignatureConfirmation(
          mintTxId,
          props.txTimeout,
          props.connection,
          "singleGossip",
          false
        );

        if (!status?.err) {
          setAlertState({
            open: true,
            message: "Congratulations! Mint succeeded!",
            severity: "success",
          });
        } else {
          setAlertState({
            open: true,
            message: "Mint failed! Please try again!",
            severity: "error",
          });
        }
      }
    } catch (error: any) {
      // TODO: blech:
      let message = error.msg || "Minting failed! Please try again!";
      if (!error.msg) {
        if (error.message.indexOf("0x138")) {
        } else if (error.message.indexOf("0x137")) {
          message = `SOLD OUT!`;
        } else if (error.message.indexOf("0x135")) {
          message = `Insufficient funds to mint. Please fund your wallet.`;
        }
      } else {
        if (error.code === 311) {
          message = `SOLD OUT!`;
          setIsSoldOut(true);
        } else if (error.code === 312) {
          message = `Minting period hasn't started yet.`;
        }
      }

      setAlertState({
        open: true,
        message,
        severity: "error",
      });
    } finally {
      if (wallet) {
        const balance = await props.connection.getBalance(wallet.publicKey);
        setBalance(balance / LAMPORTS_PER_SOL);
      }
      setIsMinting(false);
      refreshCandyMachineState();
    }
  };

  useEffect(() => {
    (async () => {
      if (wallet) {
        const balance = await props.connection.getBalance(wallet.publicKey);
        setBalance(balance / LAMPORTS_PER_SOL);
      }
    })();
  }, [wallet, props.connection]);

  useEffect(refreshCandyMachineState, [
    wallet,
    props.candyMachineId,
    props.connection,
  ]);

  return (
    <main>
       <nav className="navbar navbar-dark navbar-expand-lg py-3" style={{background: 'black'}}>
        {/* <a class="navbar-brand" href="#">Navbar</a> */}
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <div className="
            align-items-start
            container
            d-flex
            flex-column flex-lg-row
            font-weight-bold
            px-0
          ">
            <ul className="navbar-nav">
              <li className="nav-item active">
                <a className="nav-link active" href="#introducing-zilla">Introduce</a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" href="#team">Team</a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" href="#faq">FAQ</a>
              </li>
              <li className="nav-item">
                <a href="#roadmap" className="nav-link active">Roadmap</a>
              </li>
            </ul>
            <ul className="navbar-nav">
            <li className="nav-item">
              {!wallet ? (
          <ConnectButton disabled className="nav-link px-5 py-1" style={{background: 'white', color: 'black', borderRadius: '25px'}} >MINT IS ENDED</ConnectButton>
        ) : (
          <p className="px-5 py-1">Wallet : {shortenAddress(wallet.publicKey.toBase58() || "")}</p>
        )}
              </li>
              <li className="nav-item">
               
          {!wallet ? (
            <span></span>
            ) : (
              // <p className="px-5 py-1">Wallet : {shortenAddress(wallet.publicKey.toBase58() || "")}</p>
              <ConnectButton disabled className="nav-link px-5 py-1" style={{background: 'white', color: 'black', borderRadius: '25px'}} id="connect">
              MINT IS ENDED</ConnectButton>
              
              
            )}
              </li>
              
            </ul>
          </div>
        </div>
      </nav>
      <div style={{backgroundImage: 'url("./assets/1.png")', backgroundPosition: 'center'}} className="py-5" id="hero">
        <div className="container py-5">
          <div className="align-items-center row">
            <div className="col-4 col-lg-3">
              <img src="./assets/Zilla Image/nganu.png" alt="nganu" className="rounded-circle w-100" />
            </div>
            <div className="col-8 col-lg-9 d-flex flex-column">
              <span className="d-flex text-white" id="zillaExiled">
                <span>ZILLA</span>
                <span className="d-flex">
                  <span>E</span>
                  <span style={{color: '#39ff9f'}}>X</span>
                  <span>ILED</span>
                </span>
              </span>
              <span className="d-flex" id="nation">
                <span>NATION</span>
                <span className="text-white">!</span>
              </span>
              {wallet && <p className="h2" id="general">Available: {itemsAvailable}</p>}
              {wallet && <span className="h2 pt-4" id="general">Remaining: {itemsRemaining}</span>}
              {wallet && <span className="h2" id="general">Redeemed: {itemsRedeemed}</span>}
              {wallet && <p className="h3 pt-4" id="general">Balance: {(balance || 0).toLocaleString()} SOL</p>}
              
              {!wallet ? (
            <ConnectButton disabled className="font-weight-bold px-3 mt-5 py-2 rounded-pill" id="connect" >
            MINT IS ENDED</ConnectButton>
        ) : (
          <MintButton className="font-weight-bold px-3 mt-5 py-2 rounded-pill" id="connect"
            disabled={isSoldOut || isMinting || !isActive}
            onClick={onMint}
            variant="contained"
          >
            {isSoldOut ? (
              "SOLD OUT"
            ) : isActive ? (
              isMinting ? (
                <CircularProgress />
              ) : (
                "MINT"
              )
            ) : (
              <Countdown
                date={startDate}
                onMount={({ completed }) => completed && setIsActive(true)}
                onComplete={() => setIsActive(true)}
                renderer={renderCounter}
              />
            )}
          </MintButton>
        )}
            
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="py-5" id="introducing-zilla">
          <div className="container py-5">
            <h2 className="d-flex">
              <span style={{color: 'white', fontSize:'45px'}}>INTRODUCING-</span>
              <span style={{color: '#39ff9f',fontSize:'45px'}}>ZILLA</span>
            </h2>
            <div className="row">
              <div className="col-12 col-md-6">
                <div style={{ color: 'white', borderRadius: '2.5rem', minHeight: '20rem',fontSize:'20px'}} className="p-4 py-5">
                  <p className="font-weight-bold m-0">
                    Zilla was an ancient creature that live thousand years ago and was a friendly creature, 
                    they live next to human and help each other, but as the times go human begin to start conflict each other, 
                    the effect of their war was the zilla are became decrease about 999, 
                    and the remaining zilla is exile himself on a village that no one know where this place is, 
                    because of their exile they become forgotten and one day there was a plane throwing chemicals on their village, 
                    and zilla begin to evolving one by one and become a new creature that have many variety and they have a goal to survive from extinction
                  </p>
                </div>
              </div>
              <div className="col-12 col-md-6">
                <img src="./assets/Zilla Image/20211124_212759.gif" alt="x" className="rounded-lg w-100" />
              </div>
            </div>
          </div>
        </div>
        <div style={{backgroundImage: 'url("./assets/1.png")', backgroundPosition: 'center'}} className="py-5" id="team">
          <div className="container py-5">
            <h2>team</h2>
            <div className="d-flex flex-column" style={{rowGap: '2rem'}}>
              <div className="px-1 py-4 row nav nav-tabs" style={{background: 'black', borderRadius: '1.5rem'}} id="nav-tab" role="tablist">
                <a href="#hyemu" id="nav-hyemu" data-toggle="tab" className="
                align-items-center
                col-12 col-md
                d-flex
                flex-column
                nav-link
                active
              " style={{rowGap: '0.5rem'}}>
                  <img className="w-100 mb-3" style={{borderRadius: '1rem'}} src="./assets/Zilla Image/pixil-frame-0_33.png" alt="x" />
                  <h3 className="m-0" style={{fontFamily: '"godzillamoviefont"', color: 'white', lineHeight: '0px'}}>
                    HYEMU
                  </h3>
                  <p className="font-weight-light m-0 text-center text-white">
                    Project Leader
                  </p>
                </a>
                <a href="#ge" id="nav-ge" data-toggle="tab" className="
                align-items-center
                col-12 col-md
                d-flex
                flex-column
                nav-link
              " style={{rowGap: '0.5rem'}}>
                  <img className="w-100 mb-3" style={{borderRadius: '1rem'}} src="./assets/Zilla Image/pixil-frame-0_36.png" alt="x" />
                  <h3 className="m-0" style={{fontFamily: '"godzillamoviefont"', color: 'white', lineHeight: '0px'}}>
                    GE
                  </h3>
                  <p className="font-weight-light m-0 text-center text-white">
                    Head Marketing
                  </p>
                </a>
                <a href="#patt" id="nav-patt" data-toggle="tab" className="
                align-items-center
                col-12 col-md
                d-flex
                flex-column
                nav-link
              " style={{rowGap: '0.5rem'}}>
                  <img className="w-100 mb-3" style={{borderRadius: '1rem'}} src="./assets/Zilla Image/pixil-frame-0_45.png" alt="x" />
                  <h3 className="m-0" style={{fontFamily: '"godzillamoviefont"', color: 'white', lineHeight: '0px'}}>
                    PATT
                  </h3>
                  <p className="font-weight-light m-0 text-center text-white">
                    Head Designer
                  </p>
                </a>
                <a href="#dew" id="nav-dew" data-toggle="tab" className="
                align-items-center
                col-12 col-md
                d-flex
                flex-column
                nav-link
              " style={{rowGap: '0.5rem'}}>
                  <img className="w-100 mb-3" style={{borderRadius: '1rem'}} src="./assets/Zilla Image/pixil-frame-0_44.png" alt="x" />
                  <h3 className="m-0" style={{fontFamily: '"godzillamoviefont"', color: 'white', lineHeight: '0px'}}>
                    DEW
                  </h3>
                  <p className="font-weight-light m-0 text-center text-white">
                    NFT Pixel Artist
                  </p>
                </a>
                <a href="#north" id="nav-north" data-toggle="tab" className="
                align-items-center
                col-12 col-md
                d-flex
                flex-column
                nav-link
              " style={{rowGap: '0.5rem'}}>
                  <img className="w-100 mb-3" style={{borderRadius: '1rem'}} src="./assets/Zilla Image/pixil-frame-0_43.png" alt="x" />
                  <h3 className="m-0" style={{fontFamily: '"godzillamoviefont"', color: 'white', lineHeight: '0px'}}>
                    NORTH
                  </h3>
                  <p className="font-weight-light m-0 text-center text-white">
                    Web Developer
                  </p>
                </a>
                <a href="#johun" id="nav-johun" data-toggle="tab" className="
                align-items-center
                col-12 col-md
                d-flex
                flex-column
                nav-link
              " style={{rowGap: '0.5rem'}}>
                  <img className="w-100 mb-3" style={{borderRadius: '1rem'}} src="./assets/Zilla Image/pixil-frame-0_51.png" alt="x" />
                  <h3 className="m-0" style={{fontFamily: '"godzillamoviefont"', color: 'white', lineHeight: '0px'}}>
                    JOHUN
                  </h3>
                  <p className="font-weight-light m-0 text-center text-white">
                    Community Manager
                  </p>
                </a>
              </div>
              <div className="px-4 py-4 row tab-content" style={{background: 'black', borderRadius: '1.5rem'}} id="nav-tabContent">
                <div className="tab-pane fade show active" id="hyemu">
                  <div className="row">
                    <div className="col-3 col-lg-1 col-md-2 mr-2 mr-md-0">
                      {/* <div
                    style="
                      height: 5rem;
                      width: 5rem;
                      background-color: white;
                      border-radius: 1rem;
                    "
                  ></div> */}
                      <img src="./assets/Zilla Image/pixil-frame-0_33.png" alt="x" className="rounded-lg w-100" />
                    </div>
                    <div className="col-8 col-md-8 ml-lg-1 pl-0 pl-lg-0 pl-xl-0">
                      <div style={{fontFamily: '"godzillamoviefont"', color: '#39ff9f'}} className="align-items-end d-flex">
                        <h3 className="m-0">HYEMU</h3>
                        <span style={{fontFamily: 'fantasy', paddingBottom: '3px'}}>{'{'}</span>
                        <h4 className="m-0" style={{fontSize: '17px', paddingBottom: '3px'}}>
                          Project Leader
                        </h4>
                        <span style={{fontFamily: 'fantasy', paddingBottom: '3px'}}>{'}'}</span>
                      </div>
                      <p className="m-0 text-white">
                        Our Project Lead, Hyemu, has several years of experience in the crypto industry and is interested in the Solana blockchain, 
                        so he decided to create the Zilla NFT and introduced them to Solana World.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade" id="ge">
                  <div className="row">
                    <div className="col-3 col-lg-1 col-md-2 mr-2 mr-md-0">
                      {/* <div
                    style="
                      height: 5rem;
                      width: 5rem;
                      background-color: white;
                      border-radius: 1rem;
                    "
                  ></div> */}
                      <img src="./assets/Zilla Image/pixil-frame-0_36.png" alt="x" className="rounded-lg w-100" />
                    </div>
                    <div className="col-8 col-md-8 ml-lg-1 pl-0 pl-lg-0 pl-xl-0">
                      <div style={{fontFamily: '"godzillamoviefont"', color: '#39ff9f'}} className="align-items-end d-flex">
                        <h3 className="m-0">GE</h3>
                        <span style={{fontFamily: 'fantasy', paddingBottom: '3px'}}>{'{'}</span>
                        <h4 className="m-0" style={{fontSize: '17px', paddingBottom: '3px'}}>
                          Head Marketing
                        </h4>
                        <span style={{fontFamily: 'fantasy', paddingBottom: '3px'}}>{'}'}</span>
                      </div>
                      <p className="m-0 text-white">
                        GE as a marketing team and has experience in crypto for 2 years and also GE is very familiar with some Dao, GE also has experience as a team in several projects such as BrickkenSTO.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade" id="patt">
                  <div className="row">
                    <div className="col-3 col-lg-1 col-md-2 mr-2 mr-md-0">
                      {/* <div
                    style="
                      height: 5rem;
                      width: 5rem;
                      background-color: white;
                      border-radius: 1rem;
                    "
                  ></div> */}
                      <img src="./assets/Zilla Image/pixil-frame-0_45.png" alt="x" className="rounded-lg w-100" />
                    </div>
                    <div className="col-8 col-md-8 ml-lg-1 pl-0 pl-lg-0 pl-xl-0">
                      <div style={{fontFamily: '"godzillamoviefont"', color: '#39ff9f'}} className="align-items-end d-flex">
                        <h3 className="m-0">PATT</h3>
                        <span style={{fontFamily: 'fantasy', paddingBottom: '3px'}}>{'{'}</span>
                        <h4 className="m-0" style={{fontSize: '17px', paddingBottom: '3px'}}>
                          Head Designer
                        </h4>
                        <span style={{fontFamily: 'fantasy', paddingBottom: '3px'}}>{'}'}</span>
                      </div>
                      <p className="m-0 text-white">
                        Just a random guy who enjoys visual design and works on several crypto projects on Solana as part of the designer team.
                      </p>
                       <a href="https://www.deviantart.com/cheerless27">https://www.deviantart.com/cheerless27</a>
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade" id="dew">
                  <div className="row">
                    <div className="col-3 col-lg-1 col-md-2 mr-2 mr-md-0">
                      {/* <div
                    style="
                      height: 5rem;
                      width: 5rem;
                      background-color: white;
                      border-radius: 1rem;
                    "
                  ></div> */}
                      <img src="./assets/Zilla Image/pixil-frame-0_44.png" alt="x" className="rounded-lg w-100" />
                    </div>
                    <div className="col-8 col-md-8 ml-lg-1 pl-0 pl-lg-0 pl-xl-0">
                      <div style={{fontFamily: '"godzillamoviefont"', color: '#39ff9f'}} className="align-items-end d-flex">
                        <h3 className="m-0">DEW</h3>
                        <span style={{fontFamily: 'fantasy', paddingBottom: '3px'}}>{'{'}</span>
                        <h4 className="m-0" style={{fontSize: '17px', paddingBottom: '3px'}}>
                          NFT Pixel Artist
                        </h4>
                        <span style={{fontFamily: 'fantasy', paddingBottom: '3px'}}>{'}'}</span>
                      </div>
                      <p className="m-0 text-white">
                        Dew has been an artist for 5 years. He started his work by focusing on realism and surrealism. 
                        Involved in NFT since mid-2021. He is one of the designers at Grape Protocol and has won several art competitions in the NFT community, 
                        one of them is the Traits Contest at Degenerate Ape Academy in which he won the 2nd Place.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade" id="north">
                  <div className="row">
                    <div className="col-3 col-lg-1 col-md-2 mr-2 mr-md-0">
                      {/* <div
                    style="
                      height: 5rem;
                      width: 5rem;
                      background-color: white;
                      border-radius: 1rem;
                    "
                  ></div> */}
                      <img src="./assets/Zilla Image/pixil-frame-0_43.png" alt="x" className="rounded-lg w-100" />
                    </div>
                    <div className="col-8 col-md-8 ml-lg-1 pl-0 pl-lg-0 pl-xl-0">
                      <div style={{fontFamily: '"godzillamoviefont"', color: '#39ff9f'}} className="align-items-end d-flex">
                        <h3 className="m-0">NORTH</h3>
                        <span style={{fontFamily: 'fantasy', paddingBottom: '3px'}}>{'{'}</span>
                        <h4 className="m-0" style={{fontSize: '17px', paddingBottom: '3px'}}>
                          Web Developer
                        </h4>
                        <span style={{fontFamily: 'fantasy', paddingBottom: '3px'}}>{'}'}</span>
                      </div>
                      <p className="m-0 text-white">
                      North has been a programmer for 4 years. He focused on Web Development Frontend, Slicing design to web template and Backend. He worked for several companies as a freelancer 3 years ago and has won several Application Development competitions in High school. 
                      </p>
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade" id="johun">
                  <div className="row">
                    <div className="col-3 col-lg-1 col-md-2 mr-2 mr-md-0">
                      {/* <div
                    style="
                      height: 5rem;
                      width: 5rem;
                      background-color: white;
                      border-radius: 1rem;
                    "
                  ></div> */}
                      <img src="./assets/Zilla Image/pixil-frame-0_51.png" alt="x" className="rounded-lg w-100" />
                    </div>
                    <div className="col-8 col-md-8 ml-lg-1 pl-0 pl-lg-0 pl-xl-0">
                      <div style={{fontFamily: '"godzillamoviefont"', color: '#39ff9f'}} className="align-items-end d-flex">
                        <h3 className="m-0">JOHUN</h3>
                        <span style={{fontFamily: 'fantasy', paddingBottom: '3px'}}>{'{'}</span>
                        <h4 className="m-0" style={{fontSize: '17px', paddingBottom: '3px'}}>
                          Community Manager
                        </h4>
                        <span style={{fontFamily: 'fantasy', paddingBottom: '3px'}}>{'}'}</span>
                      </div>
                      <p className="m-0 text-white">
                        Johun has been involved in the cryptocurrency field for 4 years. 
                        He has been working as a community manager/moderator on a number of projects, including HaloDao, Grape Protocol, 
                        PlaySnook, Parrot Finance, and has a strong focus on the Solana ecosystem. He's also a co-founder of the Pyra community.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="py-5" id="faq">
          <div className="container">
            <h2>FAQ</h2>
            <div className="row">
              <div className="col-12 d-flex">
                <div className="align-items-center d-flex justify-content-end py-5" id="count">
                  <span className="mr-2" style={{fontFamily: '"godzillamoviefont"', fontSize: '50px', lineHeight: 0,color:"black"}}>1</span>
                </div>
                <div className="bg-white d-flex flex-column flex-grow-1 px-4" style={{marginLeft: '1px'}}>
                  <span style={{fontFamily: '"godzillamoviefont"', fontSize: '50px', lineHeight: '36px', marginTop: '20px',color:"black"}}>What is the Zilla Nation NFT</span>
                  <span style={{ color:"black", fontSize:'18px' }} className="h6">Zilla Nation NFT is the first gen nfts of the Zilla Nation NFT series, the first gen consists of 999 different Zilla.</span>
                </div>
              </div>
              <div className="col-12 d-flex">
                <div className="align-items-center d-flex justify-content-end py-5" id="count">
                  <span className="mr-2" style={{fontFamily: '"godzillamoviefont"', fontSize: '50px', lineHeight: 0,color:"black"}}>2</span>
                </div>
                <div className="bg-white d-flex flex-column flex-grow-1 px-4" style={{marginLeft: '1px'}}>
                  <span style={{fontFamily: '"godzillamoviefont"', fontSize: '50px', lineHeight: '36px', marginTop: '20px',color:"black"}}>When is the mint</span>
                  <span style={{ color:"black", fontSize:'18px' }} className="h6">19 Dec 2021, 03.00 PM UTC</span>
                </div>
              </div>
              <div className="col-12 d-flex">
                <div className="align-items-center d-flex justify-content-end py-5" id="count">
                  <span className="mr-2" style={{fontFamily: '"godzillamoviefont"', fontSize: '50px', lineHeight: 0,color:"black"}}>3</span>
                </div>
                <div className="bg-white d-flex flex-column flex-grow-1 px-4" style={{marginLeft: '1px'}}>
                  <span style={{fontFamily: '"godzillamoviefont"', fontSize: '50px', lineHeight: '36px', marginTop: '20px',color:"black"}}>What is the Total Supply</span>
                  <span style={{ color:"black", fontSize:'18px' }} className="h6">999 NFTs</span>
                </div>
              </div>
              <div className="col-12 d-flex">
                <div className="align-items-center d-flex justify-content-end py-5" id="count">
                  <span className="mr-2" style={{fontFamily: '"godzillamoviefont"', fontSize: '50px', lineHeight: 0,color:"black"}}>4</span>
                </div>
                <div className="bg-white d-flex flex-column flex-grow-1 px-4" style={{marginLeft: '1px'}}>
                  <span style={{fontFamily: '"godzillamoviefont"', fontSize: '50px', lineHeight: '36px', marginTop: '20px',color:"black"}}>What is the utility</span>
                  <span style={{ color:"black", fontSize:'15px' }} className="h6">Check out our roadmap, we explain a lot there! We also have some plans for the future, and will be releasing them on our Discord.</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div style={{backgroundImage: 'url("./assets/1.png")', backgroundPosition: 'center'}} className="py-5" id="roadmap">
          <div className="container py-5">
            <h2 className="d-flex">
              <span style={{color: 'white'}}>PROJECT-</span>
              <span style={{color: '#39ff9f'}}>ROADMAP</span>
            </h2>
            <div className="
            flex-column flex-lg-row
            mt-4
            pb-3
            px-3 px-lg-5 px-md-4
            py-lg-5
            row
          " style={{background: '#232323', borderRadius: '4rem'}}>
              <div className="
              align-items-center
              col
              d-flex
              justify-content-lg-center justify-content-start
              position-relative
            ">
                <div className="d-flex flex-column position-absolute text-white" id="text-content">
                  <div style={{fontFamily: '"godzillamoviefont"', fontSize: '30px'}}>
                    <span>PHASE</span>
                    <span style={{color: '#37ff9f'}}>0</span>
                  </div>
                  <p className="font-weight-bold m-0" style={{marginTop: '-0.5rem !important', lineHeight: '14px'}}>
                    Making a Zilla exiled Nation Community
                  </p>
                </div>
                <div id="dot" className="rounded-circle" />
                <div id="line-1" className="position-absolute" />
              </div>
              <div className="
              align-items-center
              col
              d-flex
              justify-content-lg-center justify-content-start
              position-relative
            ">
                <div className="d-flex flex-column position-absolute text-white" id="text-content-2">
                  <div style={{fontFamily: '"godzillamoviefont"', fontSize: '30px'}}>
                    <span>PHASE</span>
                    <span style={{color: '#37ff9f'}}>1</span>
                  </div>
                  <p className="font-weight-bold m-0" style={{marginTop: '-0.5rem !important', lineHeight: '14px'}}>
                    Making a Zilla exiled Nation NFT
                  </p>
                </div>
                <div id="dot" className="rounded-circle" />
                <div id="line" className="position-absolute" />
              </div>
              <div className="
              align-items-center
              col
              d-flex
              justify-content-lg-center justify-content-start
              position-relative
            ">
                <div className="d-flex flex-column position-absolute text-white" id="text-content">
                  <div style={{fontFamily: '"godzillamoviefont"', fontSize: '30px'}}>
                    <span>PHASE</span>
                    <span style={{color: '#37ff9f'}}>2</span>
                  </div>
                  <p className="font-weight-bold m-0" style={{marginTop: '-0.5rem !important', lineHeight: '14px'}}>
                    Secondary market listing.
                  </p>
                </div>
                <div id="dot" className="rounded-circle" />
                <div id="line" className="position-absolute" />
              </div>
              <div className="
              align-items-center
              col
              d-flex
              justify-content-lg-center justify-content-start
              position-relative
            ">
                <div className="d-flex flex-column position-absolute text-white" id="text-content-2">
                  <div style={{fontFamily: '"godzillamoviefont"', fontSize: '30px'}}>
                    <span>PHASE</span>
                    <span style={{color: '#37ff9f'}}>3</span>
                  </div>
                  <p className="font-weight-bold m-0" style={{marginTop: '-0.5rem !important', lineHeight: '14px'}}>
                    Making a DAO to define where the comminity to go in the future
                  </p>
                </div>
                <div id="dot" className="rounded-circle" />
                <div id="line" className="position-absolute" />
              </div>
              <div className="
              align-items-center
              col
              d-flex
              justify-content-lg-center justify-content-start
              position-relative
            ">
                <div className="d-flex flex-column position-absolute text-white" id="text-content">
                  <div style={{fontFamily: '"godzillamoviefont"', fontSize: '30px'}}>
                    <span>PHASE</span>
                    <span style={{color: '#37ff9f'}}>4</span>
                  </div>
                  <p className="font-weight-bold m-0" style={{marginTop: '-0.5rem !important', lineHeight: '14px'}}>
                    Making utility token Named ZEN
                  </p>
                </div>
                <div id="dot" className="rounded-circle" />
                <div id="line" className="position-absolute" />
              </div>
              <div className="
              align-items-center
              col
              d-flex
              justify-content-lg-center justify-content-start
              position-relative
            ">
                <div className="d-flex flex-column position-absolute text-white" id="text-content-2">
                  <div style={{fontFamily: '"godzillamoviefont"', fontSize: '30px'}}>
                    <span>PHASE</span>
                    <span style={{color: '#37ff9f'}}>5</span>
                  </div>
                  <p className="font-weight-bold m-0" style={{marginTop: '-0.5rem !important', lineHeight: '14px'}}>
                    Launch breeding nft from ZEN NFT gen 1
                  </p>
                </div>
                <div id="dot" className="rounded-circle" />
                <div id="line" className="position-absolute" />
              </div>
              <div className="
              align-items-center
              col
              d-flex
              justify-content-lg-center justify-content-start
              position-relative
            ">
                <div className="d-flex flex-column position-absolute text-white" id="text-content">
                  <div style={{fontFamily: '"godzillamoviefont"', fontSize: '30px'}}>
                    <span>PHASE</span>
                    <span style={{color: '#37ff9f'}}>6</span>
                  </div>
                  <p className="font-weight-bold m-0" style={{marginTop: '-0.5rem !important', lineHeight: '14px'}}>
                    Launch our own Marketplace
                  </p>
                </div>
                <div id="dot" className="rounded-circle" />
                <div id="line-last" className="position-absolute" />
              </div>
            </div>
          </div>
        </div>
        <div style={{backgroundColor: 'black'}} className="py-5">
          <div className="container">
            <p className="m-0 text-center text-white" style={{fontSize: '22px'}}>
              www.exiledzilla.com
            </p>
            <p className="m-0 text-center text-white" style={{fontSize: '22px'}}>
              powered by Zilla Exiled Nation Team
            </p>
          </div>
        </div>
      </div>
      <Snackbar
        open={alertState.open}
        autoHideDuration={6000}
        onClose={() => setAlertState({ ...alertState, open: false })}
      >
        <Alert
          onClose={() => setAlertState({ ...alertState, open: false })}
          severity={alertState.severity}
        >
          {alertState.message}
        </Alert>
      </Snackbar>
    </main>
  );
};

interface AlertState {
  open: boolean;
  message: string;
  severity: "success" | "info" | "warning" | "error" | undefined;
}

const renderCounter = ({ days, hours, minutes, seconds, completed }: any) => {
  return (
    <CounterText>
      {hours + (days || 0) * 24} hours, {minutes} minutes, {seconds} seconds
    </CounterText>
  );
};

export default Home;
